use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;

// Define a struct to represent a character
#[derive(Debug, PartialEq)]
struct Character {
    name: String,
    species: String,
    gender: String,
    height: String,
    weight: String,
    birth_year: String,
}

impl Character {
    // Function to create a new character from CSV record
    fn new(record: csv::StringRecord) -> Character {
        Character {
            name: record.get(0).unwrap_or("").to_string(),
            species: record.get(1).unwrap_or("").to_string(),
            gender: record.get(2).unwrap_or("").to_string(),
            height: record.get(3).unwrap_or("").to_string(),
            weight: record.get(4).unwrap_or("").to_string(),
            birth_year: record.get(5).unwrap_or("").to_string(),
        }
    }
}

// Function to read characters from CSV and return vector of characters
fn read_characters_from_csv(file_path: &str) -> Result<Vec<Character>, Box<dyn Error>> {
    let mut characters = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let character = Character::new(record);
        characters.push(character);
    }
    Ok(characters)
}

// Function to find character by name (case-sensitive)
fn find_character_by_name<'a>(characters: &'a [Character], name: &str) -> Option<&'a Character> {
    characters.iter().find(|c| c.name.to_lowercase() == name.to_lowercase())
}


fn main() {
    // Read characters from CSV
    let characters = match read_characters_from_csv("./data/characters.csv") {
        Ok(chars) => chars,
        Err(err) => {
            eprintln!("Error reading characters: {}", err);
            return;
        }
    };

    // Get character name from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <character_name>", args[0]);
        return;
    }
    let character_name = &args[1];

    // Find character by name and print information
    match find_character_by_name(&characters, character_name) {
        Some(character) => println!("{:?}", character),
        None => println!("Character not found"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_character_by_name_existing() {
        let characters = vec![
            Character {
                name: "Luke Skywalker".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "172".to_string(),
                weight: "77".to_string(),
                birth_year: "19BBY".to_string(),
            },
            Character {
                name: "Darth Vader".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "202".to_string(),
                weight: "136".to_string(),
                birth_year: "41.9BBY".to_string(),
            },
        ];

        assert_eq!(
            find_character_by_name(&characters, "Luke Skywalker"),
            Some(&Character {
                name: "Luke Skywalker".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "172".to_string(),
                weight: "77".to_string(),
                birth_year: "19BBY".to_string(),
            })
        );
    }

    #[test]
    fn test_find_character_by_name_not_existing() {
        let characters = vec![
            Character {
                name: "Luke Skywalker".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "172".to_string(),
                weight: "77".to_string(),
                birth_year: "19BBY".to_string(),
            },
            Character {
                name: "Darth Vader".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "202".to_string(),
                weight: "136".to_string(),
                birth_year: "41.9BBY".to_string(),
            },
        ];

        assert_eq!(find_character_by_name(&characters, "Yoda"), None);
    }

    #[test]
    fn test_find_character_by_name_case_insensitive() {
        let characters = vec![
            Character {
                name: "Luke Skywalker".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "172".to_string(),
                weight: "77".to_string(),
                birth_year: "19BBY".to_string(),
            },
            Character {
                name: "Darth Vader".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "202".to_string(),
                weight: "136".to_string(),
                birth_year: "41.9BBY".to_string(),
            },
        ];

        assert_eq!(
            find_character_by_name(&characters, "luke Skywalker"),
            Some(&Character {
                name: "Luke Skywalker".to_string(),
                species: "Human".to_string(),
                gender: "Male".to_string(),
                height: "172".to_string(),
                weight: "77".to_string(),
                birth_year: "19BBY".to_string(),
            })
        );
    }

}
