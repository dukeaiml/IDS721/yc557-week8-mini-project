# Week 8 Mini Project Unit Testing Report
> Oliver Chen (yc557)

## Introduction
Unit testing is a crucial aspect of software development to ensure the correctness and reliability of the code. In this report, we present the results of unit testing conducted for the Rust command-line tool developed to read and process character data from a CSV file.

## Testing Environment
- **Language**: Rust
- **Testing Framework**: Rust's built-in testing framework
- **Tools**: `cargo test` command for running tests

## Test Cases
The unit tests focus on validating the functionality of the `find_character_by_name` function, which searches for a character by name (case-sensitive) within a given vector of `Character` structs. Three test cases were designed to cover different scenarios:

1. **Existing Character**: Tests if the function correctly finds an existing character by name.
2. **Non-Existing Character**: Tests if the function returns `None` when the character does not exist in the provided vector.
3. **Case-Insensitive Search**: Tests if the function performs a case-insensitive search, ensuring it finds characters regardless of case differences.

## Results
All unit tests passed successfully, indicating that the `find_character_by_name` function operates as expected in different scenarios. Below are the outcomes of each test case:

1. **Test Case: Existing Character**
   - **Outcome**: Passed
   - **Description**: The function correctly identified an existing character by name.
   
2. **Test Case: Non-Existing Character**
   - **Outcome**: Passed
   - **Description**: The function appropriately returned `None` when the character was not found in the vector.
   
3. **Test Case: Case-Insensitive Search**
   - **Outcome**: Passed
   - **Description**: The function successfully performed a case-insensitive search, ensuring it could find characters regardless of case differences.

## Conclusion
The unit testing results demonstrate the robustness and accuracy of the `find_character_by_name` function in the Rust command-line tool. These tests validate the reliability of the character search functionality, contributing to the overall quality and correctness of the software.