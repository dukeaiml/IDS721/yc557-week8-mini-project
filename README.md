# Week 8 Mini Project
> Oliver Chen (yc557)

This project presents a Rust command-line tool for reading and processing data from a CSV file containing information about Star Wars characters. It includes functionalities for reading character data from the CSV, searching for characters by name, and outputting character information.

## Project Setup
1. Create a new Rust project using the following command. Note I used the `--bin` flag to produce the binary executable for the command line tool later.
```bash
cargo new yc557-week8-mini-project --bin
```
2. Prepare some data file used for the program. I used a csv file contained Star Wars characters and their information. Here is the format:
```csv
Name,Species,Gender,Height (cm),Weight (kg),Birth Year,Homeworld,Affiliation
Luke Skywalker,Human,Male,172,77,19BBY,Tatooine,Rebel Alliance
...
```
3. Add the following dependency to the `Cargo.toml` file.
```toml
[dependencies]
csv = "1.1.6"
```
4. Implement the program functionality in `main.rs`.

## Tool Functionality
After implementing the program, we can check the functionality of the tool. 
1. Build the program first using the following command:
```bash
cargo build
```
Here is the output:
![terminal](./images/terminal-1.png)

2. Run the following command with a String arguemnt, which is a Star Wars character's name.
```bash
./target/debug/yc557-week8-mini-project <character name>
```

Here are some commands that I ran:
![terminal](./images/terminal-3.png)

If the character is not found, here is the output:
![terminal-1](./images/terminal-4.png)

## Data Processing
The details for data processing can be found in the `main.rs` file.

## Testing implementation

### Detailed Steps
1. I implemented some unit tests to test the implementation. Here is an example. This unit test tests the functionality of finding a character whose name exists in the data.
```rust
#[test]
fn test_find_character_by_name_existing() {
    let characters = vec![
        Character {
            name: "Luke Skywalker".to_string(),
            species: "Human".to_string(),
            gender: "Male".to_string(),
            height: "172".to_string(),
            weight: "77".to_string(),
            birth_year: "19BBY".to_string(),
        },
        Character {
            name: "Darth Vader".to_string(),
            species: "Human".to_string(),
            gender: "Male".to_string(),
            height: "202".to_string(),
            weight: "136".to_string(),
            birth_year: "41.9BBY".to_string(),
        },
    ];

    assert_eq!(
        find_character_by_name(&characters, "Luke Skywalker"),
        Some(&Character {
            name: "Luke Skywalker".to_string(),
            species: "Human".to_string(),
            gender: "Male".to_string(),
            height: "172".to_string(),
            weight: "77".to_string(),
            birth_year: "19BBY".to_string(),
        })
    );
}
```

2. Run all the test cases on the terminal using the following command:
```bash
cargo test
```

Here is the output:
![terminal](./images/terminal-2.png)

### Testing Report
You can find a short testing report [here](https://gitlab.com/dukeaiml/IDS721/yc557-week8-mini-project/-/blob/master/testing-report.md?ref_type=heads).